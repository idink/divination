import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import pandas as pd

from copy import deepcopy

from slytherin.progress import ProgressBar

from .WordVectorEmbedding import WordVectorEmbedding


class CNN(nn.Module):
	def __init__(self, word_vector_embedding=None, text=None, num_filters=100, filter_sizes=None, output_dim=1, dropout=0.5):
		"""
		:type num_filters: int
		:type filter_sizes: list[int]
		:type output_dim: int
		:type dropout: float
		:type word_vector_embedding: WordVectorEmbedding
		"""
		super().__init__()
		filter_sizes = filter_sizes or [3, 4, 5]
		self.embedding = word_vector_embedding or WordVectorEmbedding(text=text)
		self.convs = nn.ModuleList(
			[
				nn.Conv2d(in_channels=1, out_channels=num_filters, kernel_size=(fs, self.embedding.embedding_dim))
				for fs in filter_sizes
			]
		)
		self.fc = nn.Linear(len(filter_sizes) * num_filters, output_dim)
		self.dropout = nn.Dropout(dropout)
		self._criterion = None

	@property
	def criterion(self):
		return self._criterion

	def forward(self, x): 						# 	[sent len, batch size]
		#print(x.size())
		#permuted = x.permute(1, 0)				# 	[batch size, sent len]
		embedded = self.embedding(x)		# 	[batch size, sent len, emb dim]
		unsqueezed = embedded.unsqueeze(1)		# 	[batch size, 1, sent len, emb dim]
		conved = [F.relu(conv(unsqueezed)).squeeze(3) for conv in self.convs]
		# 											[batch size, n_filters, sent len - filter_sizes[n]]
		pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]
		# 											pooled_n = [batch size, n_filters]
		cat = self.dropout(torch.cat(pooled, dim=1))
		# 											cat = [batch size, n_filters * len(filter_sizes)]
		return self.fc(cat)

	@staticmethod
	def binary_accuracy(predictions, actuals):
		"""
		Returns accuracy per batch, i.e. if you get 8/10 right, this returns 0.8, NOT 8
		"""
		# round predictions to the closest integer
		rounded_predictions = torch.round(torch.sigmoid(predictions))
		correct = (rounded_predictions == actuals).float()  # convert into float for division
		accuracy = correct.sum() / len(correct)
		return accuracy

	def train_me(self, iterator, device, optimizer=None, criterion=None):
		model = deepcopy(self)
		model = model.to(device)

		criterion = criterion or nn.BCEWithLogitsLoss()
		criterion = criterion.to(device)

		optimizer = optimizer or optim.Adam(self.parameters())

		bar = ProgressBar(total=len(iterator))
		epoch_loss = 0
		epoch_accuracy = 0
		loss_rate = 0
		accuracy_rate = 0
		model.train()
		for progress, batch in enumerate(iterator):
			bar.show(
				amount=progress,
				text=f'CN training with loss:{round(loss_rate, 3)}, accuracy:{round(accuracy_rate, 3)}'
			)
			optimizer.zero_grad()
			predictions = model(batch['X']).squeeze(1)
			loss = criterion(predictions, batch['y'])
			accuracy = self.binary_accuracy(predictions=predictions, actuals=batch['y'])
			loss.backward()
			optimizer.step()
			epoch_loss += loss.item()
			epoch_accuracy += accuracy.item()
			loss_rate = epoch_loss / (progress + 1)
			accuracy_rate = epoch_accuracy / (progress + 1)
		bar.show(
			amount=len(iterator),
			text=f'CN trained with loss:{round(loss_rate, 3)}, accuracy:{round(accuracy_rate, 3)}!'
		)
		model._criterion = criterion
		return {'model':model, 'accuracy':accuracy_rate, 'loss':loss_rate}

	def evaluate_me(self, iterator):
		model = deepcopy(self)
		criterion = model.criterion
		bar = ProgressBar(total=len(iterator))
		epoch_loss = 0
		epoch_accuracy = 0
		loss_rate = 0
		accuracy_rate = 0
		model.eval()
		with torch.no_grad():
			for progress, batch in enumerate(iterator):
				bar.show(
					amount=progress,
					text=f'CN evaluating with loss:{round(loss_rate, 3)}, accuracy:{round(accuracy_rate, 3)}'
				)
				predictions = model(batch['X']).squeeze(1)
				loss = criterion(predictions, batch['y'])
				accuracy = self.binary_accuracy(predictions=predictions, actuals=batch.label)
				epoch_loss += loss.item()
				epoch_accuracy += accuracy.item()
				loss_rate = epoch_loss / (progress+1)
				accuracy_rate = epoch_accuracy / (progress+1)
			bar.show(
				amount=len(iterator),
				text='CN evaluated with loss:{round(loss_rate, 3)}, accuracy:{round(accuracy_rate, 3)}!'
			)
		return {'accuracy': accuracy_rate, 'loss': loss_rate}

	def train_and_evaluate(
			self, device, num_epochs, training_iterator, validation_iterator, optimizer=None, criterion=None
	):
		model = self
		training_results = {'loss':[], 'accuracy':[], 'epoch':[]}
		test_results = {'loss':[], 'accuracy':[], 'epoch':[]}
		for epoch in range(num_epochs):
			print(f'CN epoch {epoch+1}')
			training = model.train_me(
				iterator=training_iterator, device=device, optimizer=optimizer, criterion=criterion
			)
			training_results['epoch'].append(epoch)
			training_results['loss'].append(training['loss'])
			training_results['accuracy'].append(training['accuracy'])

			model = training['model']

			test = model.evaluate_me(iterator=validation_iterator)
			test_results['epoch'].append(epoch)
			test_results['loss'].append(test['loss'])
			test_results['accuracy'].append(test['accuracy'])

		results_df = pd.concat([pd.DataFrame(training_results), pd.DataFrame(test_results)])
		return model, results_df





