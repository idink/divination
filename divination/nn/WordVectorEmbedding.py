import torch.nn as nn
import torch
from pandas import Series
from copy import deepcopy

from gobbledegook.wordvectors import Vectorizer, WordVectors, Vocabulary
from slytherin.progress import ProgressBar
from memoria import Pickler


def replace_word_embedding(embedding, vocabulary, word_vectors, echo=1):
	"""
	:type embedding: nn.Embedding
	:type vocabulary: Vocabulary
	:type word_vectors: WordVectors
	:type echo: int
	:rtype: nn.Embedding
	"""
	echo = max(0, echo)
	embedding = deepcopy(embedding)
	num_embeddings_replaced = 0
	bar = ProgressBar(total=vocabulary.size)
	progress = 0
	for word, embedding_index in vocabulary.items:
		if echo and progress/100 == progress//100:
			bar.show(amount=progress, text=f'DV {num_embeddings_replaced} embeddings replaced')
		if word.lower() in word_vectors:
			word_vector = word_vectors[word.lower()]
			glove_vector = torch.FloatTensor(word_vector.vector)
			if embedding.weight.is_cuda:
				glove_vector = glove_vector.cuda()
			embedding.weight.data[embedding_index, :].set_(glove_vector)
			num_embeddings_replaced += 1
		progress+=1
	if echo: bar.show(amount=progress, text=f'DV {num_embeddings_replaced} embeddings replaced')
	return embedding


class WordVectorEmbedding(nn.Embedding):
	def __init__(self, text=None, echo=1):
		"""
		:type text: Series
		"""
		echo = max(0, echo)
		bar = ProgressBar(total=4)
		if echo: bar.show(amount=0, text='WE preparing vectorizer ... ')
		self._vectorizer = Vectorizer.fit(x=text)
		if echo: bar.show(amount=1, text='WE preparing word vectors ... ')
		self._word_vectors =WordVectors(echo=echo-1)
		if echo: bar.show(amount=2, text='WE preparing raw embedding ... ')
		super().__init__(num_embeddings=self.vocabulary.size, embedding_dim=self.word_vectors.size)
		if echo: bar.show(amount=3, text='WE replacing weights ... ')
		new_embedding = replace_word_embedding(
			embedding=self, vocabulary=self.vocabulary, word_vectors=self.word_vectors,
			echo=echo-1
		)
		self.weight.data = new_embedding.weight.data
		if echo: bar.show(amount=4, text='WE word vector embedding ready!')

	@property
	def vectorizer(self):
		return self._vectorizer

	@property
	def word_vectors(self):
		return self._word_vectors

	@property
	def vocabulary(self):
		return self.vectorizer.vocabulary


	def save(self, path):
		Pickler.save(path=path, obj=self)

	@staticmethod
	def load(path):
		return Pickler.load(path=path)


