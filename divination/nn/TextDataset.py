from torch.utils.data import Dataset as Dataset
import torch
from pandas import Series
from copy import deepcopy

from gobbledegook.wordvectors import Vectorizer

class TextDataset(Dataset):
	def __init__(self, x, y, vectorizer):
		"""
		:type vectorizer: Vectorizer
		:type x: Series
		:type y: Series
		"""
		if len(x) != len(y): raise ValueError('x and y should have the same length!')
		self._x = vectorizer.transform(x=x)
		self._y = deepcopy(y)

	def __len__(self):
		return len(self._x)

	def __getitem__(self, index):
		X = torch.from_numpy(self._x).float()
		y = self._y[index]
		return {'X':X, 'y':y}