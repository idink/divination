from sklearn.metrics import accuracy_score, precision_score, recall_score, average_precision_score, roc_curve, auc
from slytherin.numbers import beautify_num
from .ConfusionMatrix import ConfusionMatrix
from ..prediction import Classifier

class ClassificationPerformance:
	def __init__(self, classifier, average='micro'):
		"""

		:type classifier: Classifier
		"""
		if not classifier._trained: raise SyntaxError('cannot measure the performance of an untrained classifier!')
		if not classifier._tested: raise SyntaxError('cannot measure the performance of an untested classifier!')
		y_true = classifier._y_true
		y_pred = classifier._y_pred
		y_score = classifier._y_pred_prob

		self._confusion_matrix = ConfusionMatrix(classifier=classifier)
		self._accuracy = accuracy_score(y_true=y_true, y_pred=y_pred) # (TP + TN)/(P + N)
		self._precision = precision_score(y_true=y_true, y_pred=y_pred, average=average) # TP / (TP + FP)

		self._recall = recall_score(y_true=y_true, y_pred=y_pred, average=average)
		try:
			self._average_precision = average_precision_score(y_true=y_true, y_score=y_score, average='micro')
		except:
			self._average_precision = None

		try:

			# compute ROC curve and ROC area
			self._roc_auc = {}
			self._fpr = {}
			self._tpr = {}
			for index, the_class in enumerate(classifier.classes):
				self._fpr[the_class], self._tpr[the_class], _ = roc_curve(y_true=y_true, y_score=y_score[:, index])
				self._roc_auc[the_class] = auc(self._fpr[the_class], self._tpr[the_class])
			# Compute micro-average ROC curve and ROC area
			#self._fpr["micro"], self._tpr["micro"], _ = roc_curve(y_true.ravel(), y_score.ravel())
			#self._roc_auc["micro"] = auc(self._fpr["micro"], self._tpr["micro"])
		except Exception as e:
			#print(f'ROC failed due to {e}')
			pass

	@property
	def confusion_matrix(self):
		return self._confusion_matrix

	@property
	def accuracy(self):
		return self._accuracy

	@property
	def precision(self):
		return self._precision

	@property
	def average_precision(self):
		return self._average_precision

	@property
	def roc_curve(self):
		return self._roc_curve


	def __repr__(self):
		return (
			f"accuracy: {beautify_num(self.accuracy)}, "
			f"precision: {beautify_num(self.precision)}"
		)