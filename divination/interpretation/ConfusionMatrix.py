import pandas as pd
import numpy as np
import pickle

class ConfusionMatrix:
	def __init__(self, classifier=None, y_pred=None, y_true=None, classes=None, margins=True):

		if classifier is not None:
			self._df = pd.crosstab(
				classifier._y_true, columns=classifier._y_pred,
				rownames=['Actual'], colnames=['Predicted'],
				margins=margins
			)
			self._classes = classifier.performance_classes
		else:
			self._df = pd.crosstab(
				y_true, columns=y_pred,
				rownames=['Actual'], colnames=['Predicted'],
				margins=margins
			)
			self._classes = classes

		try:
			self._total = len(y_true)
		except:
			self._total = len(classifier._y_true)

		raw_confusion_matrix = self._df.drop(columns='All').drop(labels='All')
		for the_class in self._classes:
			if the_class not in raw_confusion_matrix.columns:
				raw_confusion_matrix[the_class] = 0
		raw_confusion_matrix = raw_confusion_matrix.T
		for the_class in self._classes:
			if the_class not in raw_confusion_matrix.columns:
				raw_confusion_matrix[the_class] = 0
		raw_confusion_matrix = raw_confusion_matrix.T
		self._clean_matrix = raw_confusion_matrix

		self._FP = raw_confusion_matrix.sum(axis=0) - np.diag(raw_confusion_matrix)
		self._FN = raw_confusion_matrix.sum(axis=1) - np.diag(raw_confusion_matrix)
		self._TP = pd.Series(np.diag(raw_confusion_matrix))
		self._TP.index = self._classes
		self._TN = raw_confusion_matrix.values.sum() - (self._FP + self._FN + self._TP)
		if self._FP.min() <0 :
			raise ValueError('negative FP')

	@property
	def df(self):
		return self._clean_matrix


	@property
	def classes(self):
		return self._classes

	@property
	def false_positive(self):
		return dict(self._FP)
	FP = false_positive

	@property
	def false_negative(self):
		return dict(self._FN)
	FN = false_negative

	@property
	def true_positive(self):
		return dict(self._TP)
	TP = true_positive

	@property
	def true_negative(self):
		return dict(self._TN)
	TN = true_negative


	@property
	def sensitivity(self):
		return dict(self._TP/(self._TP + self._FN))
	true_positive_rate = sensitivity
	hit_rate = sensitivity
	recall = sensitivity
	TPR = true_positive_rate

	@property
	def specificity(self):
		return dict(self._TN/(self._TN + self._FP))
	true_negative_rate = specificity
	TNR = true_negative_rate

	@property
	def precision(self):
		return dict(self._TP/(self._TP + self._FP))
	positive_predictive_value = precision
	PPV = positive_predictive_value

	@property
	def negative_predictive_value(self):
		return dict(self._TN/(self._TN + self._FN))
	NPV = negative_predictive_value

	@property
	def fall_out(self):
		return dict(self._FP/(self._FP + self._TN))
	false_positive_rate = fall_out
	FPR = false_positive_rate

	@property
	def false_negative_rate(self):
		return dict(self._FN/(self._TP + self._FN))
	FNR = false_negative_rate

	@property
	def false_discovery_rate(self):
		return dict(self._FP/(self._TP + self._FP))
	FDR = false_discovery_rate

	def __repr__(self):
		return self._df.to_string()

	def to_dict(self, key):
		return {
			'total': self._total,
			'false_positive': self.false_positive[key],
			'false_negative': self.false_negative[key],
			'true_positive': self.true_positive[key],
			'true_negative': self.true_negative[key],
			'recall': self.recall[key],
			'specificity': self.specificity[key],
			'precision': self.precision[key],
			'negative_predictive_value': self.negative_predictive_value[key],
			'fall_out': self.fall_out[key],
			'false_negative_rate': self.false_negative_rate[key],
			'false_discovery_rate': self.false_discovery_rate[key]
		}
