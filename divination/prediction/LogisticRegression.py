from sklearn import linear_model
import numpy as np
import scipy.stats as stats
from ..interpretation import get_logit_pvalues

class LogisticRegression:
	"""
	Wrapper Class for Logistic Regression which has the usual sklearn instance
	in an attribute self.model, and pvalues, z scores and estimated
	errors for each coefficient in

	self.z_scores
	self.p_values
	self.sigma_estimates

	as well as the negative hessian of the log Likelihood (Fisher information)

	self.F_ij
	"""

	def __init__(self, *args, **kwargs):  # ,**kwargs):
		self._model = linear_model.LogisticRegression(*args, **kwargs)  # ,**args)
		self._z_scores = None
		self._p_values = None
		self._sigma_estimates = None
		self._F_ij = None
		self._confidence_intervals = None

	@property
	def z_scores(self):
		return self._z_scores

	@property
	def p_values(self):
		return self._p_values

	@property
	def sigma_estimates(self):
		return self._sigma_estimates

	@property
	def F_ij(self):
		return self._F_ij

	@property
	def confidence_intervals(self):
		return self._confidence_intervals

	@property
	def model(self):
		return self._model

	def fit(self, X, y):
		self._model.fit(X, y)
		self._p_values = get_logit_pvalues(model=self._model, X=X)

		try:
			#### Get p-values for the fitted model ####
			denom = (2.0 * (1.0 + np.cosh(self.model.decision_function(X))))
			self._F_ij = np.dot((X / denom[:, None]).T, X)  ## Fisher Information Matrix
			print(self._F_ij.shape)
			Cramer_Rao = np.linalg.inv(self._F_ij)  ## Inverse Information Matrix
			self._sigma_estimates = np.sqrt(np.diagonal(Cramer_Rao))
			self._z_scores = self._model.coef_[0] / self._sigma_estimates  # z-score for eaach model coefficient
			self._p_values = [stats.norm.sf(abs(x)) * 2 for x in self._z_scores]  ### two tailed test for p-values

			alpha = 0.05
			q = stats.norm.ppf(1 - alpha / 2)
			lower = self.model.coef_[0] - q * self._sigma_estimates
			upper = self.model.coef_[0] + q * self._sigma_estimates
			self._confidence_intervals = np.dstack((lower, upper))[0]
		except:
			pass

	def predict(self, X):
		return self._model.predict(X=X)