from copy import deepcopy
import pandas as pd
from sklearn.feature_selection import f_regression
import pickle
import warnings

from slytherin.collections import has_duplicates, get_duplicates
from slytherin.time import Timer

from ..preprocessing.polynomialtransformer import PolynomialTransformer
from ..preprocessing.Normalizer import Normalizer
from ..interpretation import get_coefficients as get_coefs
from ..interpretation import get_feature_importances as get_importances


class Predictor:
	def __init__(
			self, predictive_model, name = 'predictor',
			polynomial_model = PolynomialTransformer(degree=1), normalizer = Normalizer()
	):
		self._untrained_model = deepcopy(predictive_model)
		self._untrained_poly = deepcopy(polynomial_model)
		self._untrained_normalizer = deepcopy(normalizer)
		self._name = name
		self.reset()


	def reset(self):
		self._trained = False
		self._tested = False
		self._y_true = None
		self._y_pred = None
		self._x_cols = None
		self._y_col = None
		self._model = deepcopy(self._untrained_model)
		self._poly = deepcopy(self._untrained_poly)
		self._normalizer = deepcopy(self._untrained_normalizer)
		self._coefficients = None
		self._features = None
		self._num_features = None
		self._significance = None
		self._training_size = None
		self._test_size = None
		self._training_time = None
		self._prediction_time = None
		self._performance = None


	# properties
	@property
	def x_cols(self):
		return self._x_cols

	@x_cols.setter
	def x_cols(self, x_cols):
		if has_duplicates(list(x_cols)):
			raise ValueError('x_cols has duplicates:', get_duplicates(list(x_cols)))
		self._x_cols = list(x_cols)

	@property
	def y_col(self):
		return self._y_col

	@y_col.setter
	def y_col(self, y_col):
		if type(y_col) is str:
			self._y_col = y_col
		else:
			raise TypeError('y_col should be a string. it is a', type(y_col))

	@property
	def model(self):
		return self._model

	@property
	def name(self):
		return self._name

	@property
	def prediction_time(self):
		return self._prediction_time

	@property
	def training_time(self):
		return self._training_time

	@property
	def features(self):
		return self._features

	@property
	def num_features(self):
		return self._num_features

	@property
	def significance(self):
		return self._significance

	@property
	def training_size(self):
		return self._training_size

	@property
	def test_size(self):
		return self._test_size


	def _learn_structure(self, data=None, x_cols=None, y_col=None, X=None, y=None):
		if data is not None and x_cols is not None and y_col is not None: # data, x_cols, y_col
			pass
		elif data is not None and x_cols is not None: # data, x_cols --> y_col
			_all_cols = list(data.columns)
			_y_cols = [_y for  _y in _all_cols if _y not in x_cols]
			y_col = _y_cols[0]
		elif data is not None and y_col is not None: # data, y_col
			_all_cols = list(data.columns)
			x_cols = [_x for _x in _all_cols if _x != y_col]
		elif X is not None and y is not None: # # X, y
			x_cols = list(X.columns)
			try:
				y_col = y.name
			except:
				y_col = list(y.columns)[0]
			data = pd.concat(objs=[X, y], axis=1)
		else:
			raise SyntaxError('missing arguments!')
		data = data.copy()
		X = data[x_cols]
		y = data[y_col]

		self.x_cols = x_cols
		self.y_col = y_col
		return {'data':data, 'X':X, 'y':y}

	@property
	def trained(self):
		return self._trained


	def train(self, data=None, x_cols=None, y_col=None, X=None, y=None, echo=0):
		if self._trained:
			raise SyntaxError('you cannot train a model that is already trained!')
		echo = max(0, echo)
		timer = Timer()
		prepared_data = self._learn_structure(data=data, x_cols=x_cols, y_col=y_col, X=X, y=y)
		data = prepared_data['data']
		X = prepared_data['X']
		y = prepared_data['y']

		# polynomial transformation
		if self._poly is None:
			x_poly = X.copy()
		else:
			x_poly = self._poly.fit_transform(X=X)

		# normalization
		if self._normalizer is not None:
			self._normalizer.normalize(data=x_poly, inplace=True, echo=echo)
		if echo: print('PR training ... ', end='')
		self.model.fit(X=x_poly, y=y)
		if echo: print('done!')
		self._training_time = timer.get_elapsed()

		self._num_features = x_poly.shape[1]
		self._training_size = x_poly.shape[0]
		self._trained = True

		# coefficiencts / important features
		try:
			self._coefficients = get_coefs(
				data=data, polynomial=self._poly, model=self.model,
				normalizer=self._normalizer
			)
		except:
			pass

		# p values
		try:
			with warnings.catch_warnings():
				warnings.simplefilter("ignore")
				f, p = f_regression(X=x_poly, y=y)
			self._significance = pd.DataFrame({'feature': list(self._poly.get_feature_names()), 'f_value':f, 'p_value':p})
			self._significance['level'] = pd.cut(x = self._significance['p_value'], bins=[0, 0.001, 0.005, 0.01, 0.05, float('inf')], labels=['***', '**', '*', '.', ''])
		except:
			pass

		try:
			self._features = get_importances(data=data, model=self.model, polynomial=self._poly)
		except:
			pass

	fit = train

	def preprocess(self, data, echo=False):
		data = data.copy()
		x_test = data[self.x_cols]
		if self._poly is None:
			x_test_poly = x_test
		else:
			x_test_poly = self._poly.transform(X=x_test)
		if self._normalizer is not None:
			self._normalizer.normalize(data=x_test_poly, inplace=True, echo=echo)
		return x_test_poly

	def predict(self, data, echo=False):
		if not self._trained: raise SyntaxError('you cannot use an untrained model for prediction!')
		timer = Timer()
		x_test_poly = self.preprocess(data=data, echo=echo)
		self._y_pred = self.model.predict(X=x_test_poly)
		self._prediction_time = timer.get_elapsed()
		return self._y_pred

	def test(self, data, y_true_col=None, y_true=None, echo=False):
		if not self._trained: raise SyntaxError('you cannot use an untrained model for test!')
		if y_true_col is None and y_true is None: y_true_col = self.y_col
		if y_true is None: y_true = data[y_true_col]

		self.predict(data=data)
		self._test_size = data.shape[0]
		if self._y_pred.shape != y_true.shape:
			raise ValueError('y_pred is', self._y_pred.shape, 'but y_true is', y_true.shape)
		self._y_true = y_true
		self._performance = None
		self._tested = True
		return self._y_pred

	def get_summary_row(self):
		if not self._tested:
			raise SyntaxError('you cannot get the summary of an untested model!')
		if not self._trained:
			raise SyntaxError('you cannot get the summary of an untrained model!')
		return pd.DataFrame(data={
			'name': self.name,
			'num_features': self.num_features,
			'training_size': self.training_size,
			'training_time': self.training_time,
			'test_size': self.test_size,
			'test_time': self.prediction_time
		}, index=[0])

	def save_model(self, path, only_if_trained=True):
		"""
		:type path: str
		"""
		if self._trained or not only_if_trained:
			with open(file=path, mode='wb') as output:
				pickle.dump(obj=self.model, file=output, protocol=pickle.HIGHEST_PROTOCOL)
		else:
			raise ValueError('cannot save an untrained model!')

	def load_model(self, path):
		"""
		:type path: str
		"""
		with open(file=path, mode='rb') as input:
			model = pickle.load(file=input)
		self._model = model
		self._trained = True

	def save(self, path):
		if self._trained:
			with open(file=path, mode='wb') as output:
				pickle.dump(obj=self, file=output, protocol=pickle.HIGHEST_PROTOCOL)
		else:
			raise ValueError('cannot save an untrained predictor!')

	@classmethod
	def load(cls, path):
		"""
		:type path: str
		:rtype: Predictor
		"""
		with open(file=path, mode='rb') as input:
			predictor = pickle.load(file=input)
		if isinstance(predictor, cls):
			return predictor
		else:
			raise TypeError(f'the object at "{path}" is a "{type(predictor)}"')
