
from slytherin.time import Timer
import pandas as pd
from .Predictor import Predictor
from ..interpretation.ClassificationPerformance import ClassificationPerformance
from ..preprocessing.polynomialtransformer import PolynomialTransformer
from ..preprocessing.Normalizer import Normalizer

class Classifier(Predictor):
	def __init__(
			self, predictive_model, name='classifier',
			polynomial_model = PolynomialTransformer(degree=1), normalizer = Normalizer(),
			threshold = None,
	):
		super().__init__(predictive_model=predictive_model, name=name, polynomial_model=polynomial_model, normalizer=normalizer)
		self._y_pred_prob = None
		self._y_pred_prob_df = None
		self._threshold = threshold
		self._performance_classes = None

	@property
	def threshold(self):
		return self._threshold

	@property
	def classes(self):
		return self.model.classes_

	@property
	def performance_classes(self):
		return self._performance_classes

	def predict_probability(self, data, echo=0):
		echo = max(0, echo)
		if echo: print('CL predicting probability ... ', end='')
		timer = Timer()
		x_test_poly = self.preprocess(data=data, echo=echo)
		try:
			self._y_pred_prob = self.model.predict_proba(X=x_test_poly)
			self._y_pred_prob_df = pd.DataFrame(self._y_pred_prob, columns=self.model.classes_)
			self._prediction_time = timer.get_elapsed()
			if echo: print('done!')
			return self._y_pred_prob_df
		except Exception as e:
			print(f'Warning! {e}')



	def predict(self, data, threshold=None, pred_class=None, echo=0):
		echo = max(0, echo)
		if not self._trained: raise SyntaxError('you cannot use an untrained model for prediction!')
		if threshold is None: threshold = self._threshold
		if threshold is None:
			self._performance_classes = self.classes
			timer = Timer()
			x_test_poly = self.preprocess(data=data, echo=echo)
			self._y_pred = self.model.predict(X=x_test_poly)
			self.predict_probability(data=data, echo=echo)
			if self._y_pred_prob_df is not None:
				self._y_pred_prob_df['prediction'] = self._y_pred
			self._prediction_time = timer.get_elapsed()
			return self._y_pred
		else:
			if pred_class is None: pred_class = self.classes[0]
			self._performance_classes = [True, False]
			self._y_pred = (self.predict_probability(data=data, echo=echo)[pred_class] > threshold).values
			self._y_pred_prob_df['prediction'] = self._y_pred
			return self._y_pred


	def test(self, data, threshold=None, pred_class=None, y_true_col=None, y_true=None, echo=0):
		echo = max(0, echo)
		if not self._trained: raise SyntaxError('you cannot use an untrained model for test!')
		if threshold is None: threshold = self._threshold
		if y_true_col is None and y_true is None: y_true_col = self.y_col
		if y_true is None: y_true = data[y_true_col]
		if pred_class is None: pred_class = self.classes[0]
		if threshold is None:
			self.predict_probability(data=data, echo=echo) # otherwise probability has been predicted already
			self._y_true = y_true
		else:
			self._y_true = y_true==pred_class

		self.predict(data=data, threshold=threshold, pred_class=pred_class, echo=echo)
		self._test_size = data.shape[0]
		if self._y_pred.shape != y_true.shape:
			raise ValueError('y_pred is', self._y_pred.shape, 'but y_true is', y_true.shape)

		self._performance = None
		self._tested = True
		return self._y_pred


	@property
	def performance(self):
		if self._performance is None:
			self._performance = ClassificationPerformance(classifier=self)
		return self._performance

	def get_summary_row(self):
		if not self._tested:
			raise SyntaxError('you cannot get the summary of an untested model!')
		if not self._trained:
			raise SyntaxError('you cannot get the summary of an untrained model!')
		return pd.DataFrame(data={
			'name': self.name,
			'num_features':self.num_features,
			'training_size':self.training_size,
			'training_time':self.training_time,
			'test_size':self.test_size,
			'test_time':self.prediction_time,
			'accuracy':self.performance.accuracy,
			'precision':self.performance.precision
		}, index=[0])

class MulticlassClassifier(Classifier):
	pass

class BinaryClassifier(Classifier):
	def get_summary_row(self):
		return pd.DataFrame(data={
			'name': self.name,
			'num_features': self.num_features,
			'training_size': self.training_size,
			'training_time': self.training_time,
			'test_size': self.test_size,
			'test_time': self.prediction_time,
			'accuracy': self.performance.accuracy,
			'precision': self.performance.precision
		}, index=[0])