from setuptools import setup, find_packages

setup(
	name='divination',
	version='2.0',
	description='Methods for predicting future',
	license='GNU AGPLv3',
	url='',
	author='Idin',
	author_email='d@idin.net',

	classifiers=[
		'Development Status :: 3 - Alpha',
		'Intended Audience :: Developers',
		'Programming Language :: Python :: 3 :: Only',
		'Programming Language :: Python :: 3.6',
		'Programming Language :: Python :: 3.7',
		'Topic :: Software Development :: Libraries :: Python Modules'
	],
	packages=find_packages(exclude=("jupyter_tests", ".idea", ".git")),
	install_requires=['pandas', 'numpy', 'sklearn', 'scipy', 'torch', 'vocabulary', 'slytherin', 'gobbledegook', 'memoria'],
	python_requires='~=3.6',
	zip_safe=True
)